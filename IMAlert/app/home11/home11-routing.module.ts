import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Home11Page } from './home11.page';

const routes: Routes = [
  {
    path: '',
    component: Home11Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Home11PageRoutingModule {}
