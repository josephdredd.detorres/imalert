import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Home11PageRoutingModule } from './home11-routing.module';

import { Home11Page } from './home11.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Home11PageRoutingModule
  ],
  declarations: [Home11Page]
})
export class Home11PageModule {}
