import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home2',
    loadChildren: () => import('./home2/home2.module').then( m => m.Home2PageModule)
  },
  {
    path: 'home3',
    loadChildren: () => import('./home3/home3.module').then( m => m.Home3PageModule)
  },
  {
    path: 'home4',
    loadChildren: () => import('./home4/home4.module').then( m => m.Home4PageModule)
  },
  {
    path: 'home5',
    loadChildren: () => import('./home5/home5.module').then( m => m.Home5PageModule)
  },
  {
    path: 'home6',
    loadChildren: () => import('./home6/home6.module').then( m => m.Home6PageModule)
  },
  {
    path: 'home7',
    loadChildren: () => import('./home7/home7.module').then( m => m.Home7PageModule)
  },
  {
    path: 'home8',
    loadChildren: () => import('./home8/home8.module').then( m => m.Home8PageModule)
  },
  {
    path: 'home9',
    loadChildren: () => import('./home9/home9.module').then( m => m.Home9PageModule)
  },
  {
    path: 'home10',
    loadChildren: () => import('./home10/home10.module').then( m => m.Home10PageModule)
  },
  {
    path: 'home11',
    loadChildren: () => import('./home11/home11.module').then( m => m.Home11PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
